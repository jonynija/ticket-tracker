# Ticket Tracker App

The Ticket Tracker app is a Swift UI project that helps you manage and track tickets or tasks effectively. This project is built using SwiftUI, providing a modern and user-friendly interface.

> This app is focused to work on iPad, but it can also be tested on iPhone and macOS

> This app is available for light and dark mode

## Features

-   **Ticket Listing**: View a list of all your tickets with essential details.
-   **Ticket Details**: Click on a ticket to view its details, including description, status, and due date.
-   **Add New Ticket**: Easily add new tickets with relevant information.
-   **Edit and Update**: Modify existing tickets, update their details
-   **Delete**: Delete ticket as needed
-   **Get Directions**: A clean and intuitive user interface for locations
-   **Auth**: Feature to login and mantain sesion

## Getting Started

To run this project locally, follow these steps:

1. Clone the repository:

```bash
git clone https://gitlab.com/jonynija/ticket-tracker.git
```

2. Because the project uses Pod, is needed to install pods with `pod install --repo-update`
3. Remember to open `TicketTracker.xcworkspace`
4. Build and run the project on your preferred simulator or device.

## Dependencies

-   **SwiftUI:**
    > Apple's declarative UI framework for building modern interfaces across Apple platforms.
-   **SQLite.swift:**
    > Swift wrapper for SQLite, enabling type-safe and Swift-friendly interactions with SQLite databases.
-   **FirebaseAnalytics:**
    > Part of Firebase by Google, it provides analytics tools for tracking user engagement and behavior in the app.
-   **Alamofire:**
    > A popular Swift-based HTTP networking library simplifying network requests and interactions with web services.
-   **GoogleMaps:**
    > Google Maps SDK for iOS allows integration of Google Maps, enabling map-related features in the app.
-   **KeychainSwift:**
    > A Swift wrapper for Keychain services, facilitating secure storage of sensitive data like credentials.
-   **NotificationBannerSwift:**
    > A Swift library for displaying customizable in-app notification banners to keep users informed.

## Screenshots

### Auth

<img src="readme/login.png" width="300" alt="Login">
<img src="readme/register.png" width="300" alt="Register">

### Tickets

<img src="readme/dashboard.png" width="300" alt="Dashboard">
<img src="readme/dashboard_menu.png" width="300" alt="Dashboard Menu">
<img src="readme/dashboard_options.png" width="300" alt="Dashboard Options">
<img src="readme/appointments.png" width="300" alt="Appointments">
<img src="readme/new_ticket.png" width="300" alt="New Ticket">
<img src="readme/update.png" width="300" alt="Update">

### Get directions

<img src="readme/get_directions.png" width="300" alt="Get Directions">
