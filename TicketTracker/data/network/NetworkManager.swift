//
//  NetworkManager.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation
import Alamofire

class NetworkManager {
    static let shared = NetworkManager()

    private init() {}

    func request<T: Decodable>(_ request: APIRequest, completion: @escaping (Result<T, Error>) -> Void) {
        AF.request(request).responseDecodable(of: T.self) { response in
            print(response)
            
            switch response.result {
            case .success(let value):
                completion(.success(value))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
