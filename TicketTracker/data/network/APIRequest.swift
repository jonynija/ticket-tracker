//
//  APIRequest.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation
import Alamofire

protocol APIRequest: URLRequestConvertible {
    var baseURL: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
}


extension APIRequest {
    func buildAsURLRequest() throws -> URLRequest {
        var urlRequest = try URLRequest(url: baseURL.appendingPathComponent(path), method: method)
        urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        
        print(urlRequest)
        
        return urlRequest
    }
}
