//
//  PredictionsResponse.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import Foundation

struct PredictionsResponse: Decodable {
    let predictions: [AddressPrediction]
    let status: String

    var validResult: Bool {
        return (status == "OK" || status == "ZERO_RESULTS")
    }
}
