//
//  PlaceResponse.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import Foundation

struct PlaceResponse: Decodable {
    let results: [PlaceResult]
    let status: String
    
    var validResult: Bool {
        return (status == "OK" || status == "ZERO_RESULTS")
    }
}

struct PlaceResult: Decodable {
    let geometry: Geometry
}
