//
//  PickLocationRepository.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation
import GoogleMaps

protocol PickLocationRepository {
    func lookForLocation(address: String, completion: @escaping (Result<PredictionsResponse, Error>) -> Void)
    func getPlaceDetails(placeId: String, completion: @escaping (Result<PlaceResponse, Error>) -> Void)
}

class PickLocationRepositoryImpl: PickLocationRepository {
    private let networkManager: NetworkManager

    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }

    func lookForLocation(address: String, completion: @escaping (Result<PredictionsResponse, Error>) -> Void) {
        let request = PickLocationRequest(address: address)

        networkManager.request(request) { (result: Result<PredictionsResponse, Error>) in
            switch result {
            case .success(let response):
                completion(.success(response))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getPlaceDetails(placeId: String, completion: @escaping (Result<PlaceResponse, Error>) -> Void) {
        let request = GetPlaceDetailsRequest(placeId: placeId)

        networkManager.request(request) { (result: Result<PlaceResponse, Error>) in
            switch result {
            case .success(let response):
                completion(.success(response))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
