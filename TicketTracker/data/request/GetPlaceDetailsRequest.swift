//
//  GetPlaceDetailsRequest.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import Foundation
import Alamofire

struct GetPlaceDetailsRequest: APIRequest {
    let baseURL: URL
    let path: String = "/maps/api/geocode/json"
    let method: HTTPMethod = .get
    let parameters: Parameters?
    
    init(placeId: String) {
        self.baseURL = URL(string: "https://maps.googleapis.com")!
        self.parameters = [
            "place_id": placeId,
            "key": "AIzaSyDoTnf4k5QTL_ZPU4wqsGjHNSk1dqWr5nw"
        ]
    }
    
    func asURLRequest() throws -> URLRequest {
        return try buildAsURLRequest()
    }
}
