//
//  PickLocationRequest.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation
import Alamofire

struct PickLocationRequest: APIRequest {
    let baseURL: URL
    let path: String = "/maps/api/place/autocomplete/json"
    let method: HTTPMethod = .get
    let parameters: Parameters?
    
    init(address: String) {
        self.baseURL = URL(string: "https://maps.googleapis.com")!
        self.parameters = [
            "input": address,
            "key": "AIzaSyDoTnf4k5QTL_ZPU4wqsGjHNSk1dqWr5nw"
        ]
    }
    
    func asURLRequest() throws -> URLRequest {
        return try buildAsURLRequest()
    }
}
