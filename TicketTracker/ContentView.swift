//
//  ContentView.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 7/01/24.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var rootBloc: RootBloc = RootBloc()
    
    init() {
        rootBloc.initialCheck()
    }
    
    var body: some View {
        if rootBloc.isLoading {
            ProgressView()
        } else if rootBloc.isUserLogged {
            TicketsPage{
                rootBloc.checkAuth()
            }
        } else {
            LoginPage{
                rootBloc.checkAuth()
            }
        }
    }
}

class RootBloc: ObservableObject {
    
    @Published var isUserLogged: Bool = false
    @Published var isLoading: Bool = true
    
    func checkAuth(){
        isLoading = false
        isUserLogged = (AppAuthHandler.shared.isUserLogged() != nil)
        print("Checking user \(isUserLogged)")
    }
    
    func initialCheck() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.checkAuth()
        }
    }
}

#Preview {
    ContentView()
}
