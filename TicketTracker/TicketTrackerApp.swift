//
//  TicketTrackerApp.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 7/01/24.
//

import SwiftUI
import FirebaseCore
import GoogleMaps

@main
struct TicketTrackerApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}


class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        FirebaseApp.configure()
        GMSServices.provideAPIKey("AIzaSyBoKVsVzY5XIboa8gGtPxcRL_l5ozk-50w")
        
        return true
    }
}