//
//  TicketsTableManager.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import Foundation
import SQLite

struct TicketsTableManager: TableProtocol {
    let database: Connection
    let tableName: String =  "Tickets"
    private let tickets: Table
    
    let idRow = Expression<Int64>("id")
    private let dateRow = Expression<Date>("date")
    private let clientRow = Expression<String>("client")
    private let addressRow = Expression<String>("address")
    
    
    init(db database: Connection) {
        self.database = database
        self.tickets = Table(tableName)
    }
    
    func createTable() throws {
        guard !isTableCreated() else {
            print("Table \(tableName) already exists")
            return
        }
        
        do {
            try database.run(tickets.create { table in
                table.column(idRow, primaryKey: .autoincrement)
                table.column(dateRow)
                table.column(clientRow)
                table.column(addressRow)
            })
            
            print("Table Created: \(tableName)")
        } catch {
            throw error
        }
    }
    
    func getAllTickets() -> [Ticket] {
        var tasks: [Ticket] = []
        
        do {
            for task in try database.prepare(self.tickets) {
                tasks.append(
                    Ticket(
                        id: task[idRow],
                        date: task[dateRow],
                        client: task[clientRow],
                        address: task[addressRow]
                    )
                )
            }
        } catch {
            print(error)
        }
        
        return tasks
    }
    
    func insert(ticket: Ticket) -> Int64? {
        let insert = tickets.insert(self.dateRow <- ticket.date,
                                    self.clientRow <- ticket.client,
                                    self.addressRow <- ticket.address
        )
        
        do {
            return try database.run(insert)
        } catch {
            print(error)
        }
        
        return nil
    }
    
    func delete(id: Int64) -> Bool {
        do {
            let ticketRow = tickets.filter(self.idRow == id)
            try database.run(ticketRow.delete())
            return true
        } catch {
            print(error)
        }
        
        return false
    }
    
    func update(ticket: Ticket) -> Bool {
        let ticketRow = tickets.filter(self.idRow == ticket.id)
        
        do {
            let update = ticketRow.update(self.dateRow <- ticket.date,
                                          self.clientRow <- ticket.client,
                                          self.addressRow <- ticket.address)
            
            if try database.run(update) > 0 {
                return true
            }
        } catch {
            print(error)
        }
        
        return false
    }
}
