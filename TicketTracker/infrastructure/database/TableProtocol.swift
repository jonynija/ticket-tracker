//
//  Table Protocol.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import Foundation
import SQLite

protocol TableProtocol {
    var database: Connection { get }
    var tableName: String { get }
    var idRow: Expression<Int64> { get }
    func createTable() throws
}


extension TableProtocol {
    func isTableCreated() -> Bool {
        do {
            let count = try database.scalar(
                "SELECT COUNT(*) FROM sqlite_master WHERE type = 'table' AND name = ?",
                tableName
            ) as? Int64 ?? 0
            return count > 0
        } catch {
            print("Error checking table existence: \(error)")
            return false
        }
    }
}
