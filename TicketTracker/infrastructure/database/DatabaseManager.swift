//
//  DatabaseManager.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation
import SQLite

class DatabaseManager {
    
    // MARK: Singleton
    static let shared = DatabaseManager()
    
    // MARK: Constants
    
    static let DIR_TASK_DB = "TicketsDB"
    static let STORE_NAME = "task.sqlite3"
    
    // MARK: Vars
    
    private var db: Connection? = nil
    
    var tickets: TicketsTableManager?
    var users: UserTableManager?
    
    private init() {
        if let docDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let dirPath = docDir.appendingPathComponent(Self.DIR_TASK_DB)
            
            do {
                try FileManager.default.createDirectory(atPath: dirPath.path, withIntermediateDirectories: true, attributes: nil)
                
                let dbPath = dirPath.appendingPathComponent(Self.STORE_NAME).path
                db = try Connection(dbPath)
                createTables()
                print("SQLiteDataStore init successfully at: \(dbPath) ")
            } catch {
                db = nil
                print("SQLiteDataStore init error: \(error)")
            }
        } else {
            db = nil
        }
    }
    
    private func createTables() {
        guard let database = db else {
            return
        }
        
        do {
            tickets = TicketsTableManager(db: database)
            try tickets?.createTable()
        } catch {
            print("Error creating tables: \(error)")
        }
        
        do {
            users = UserTableManager(db: database)
            try users?.createTable()
        } catch {
            print("Error creating tables: \(error)")
        }
    }
}
