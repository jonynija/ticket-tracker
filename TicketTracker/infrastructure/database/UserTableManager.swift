//
//  UserTableManager.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import Foundation
import SQLite

struct User {
    var id: Int64
    var username: String
    var password: String
}

class UserTableManager: TableProtocol {
    let database: Connection
    let tableName: String =  "Users"
    private let users: Table
    
    let idRow = Expression<Int64>("id")
    private let usernameRow = Expression<String>("username")
    private let passwordRow = Expression<String>("password")
    
    init(db database: Connection) {
        self.database = database
        self.users = Table(tableName)
    }
    
    func createTable() throws {
        guard !isTableCreated() else {
            print("Table \(tableName) already exists")
            return
        }
        
        do {
            try database.run(users.create { table in
                table.column(idRow, primaryKey: true)
                table.column(usernameRow, unique: true)
                table.column(passwordRow)
            })
            
            print("Table Created: \(tableName)")
        } catch {
            throw error
        }
    }
    
    func createUser(username: String, password: String) -> Int64? {
        do {
            let insert = users.insert(self.usernameRow <- username, self.passwordRow <- password)
            return try database.run(insert)
        } catch {
            print("Error creating user: \(error)")
        }
        
        return nil
    }
    
    func getUser(username: String, password: String) -> User? {
        do {
            let query = users.filter(self.usernameRow == username && self.passwordRow == password)
            if let userRow = try database.pluck(query) {
                return User(id: userRow[idRow], username: userRow[self.usernameRow], password: userRow[self.passwordRow])
            }
        } catch {
            print("Error getting user: \(error)")
        }
        return nil
    }
}
