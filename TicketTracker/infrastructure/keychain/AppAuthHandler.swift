//
//  AppAuthHandler.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import Foundation
import KeychainSwift

struct AppAuthHandler {
    
    // MARK: Singleton
    static let shared = AppAuthHandler()
    
    
    private let keychain = KeychainSwift()
    
    // MARK: Keys
    private static let userLoggedKey = "IsUserLogged"
    private static let userIdKey = "UserIdKey"
    
    // MARK: Methods
    
    func saveUser(id: Int64) {
        keychain.set(true, forKey: AppAuthHandler.userLoggedKey)
        keychain.set(String(id), forKey: AppAuthHandler.userIdKey)
    }
    
    func isUserLogged() -> Int64? {
        let isUserLogged = keychain.getBool(AppAuthHandler.userLoggedKey) ?? false
        
        if !isUserLogged{
            return nil
        }
        
        if let userId = keychain.get(AppAuthHandler.userIdKey){
            return Int64(userId)
        }
        
        return nil
    }
    
    func logout() {
        keychain.delete(AppAuthHandler.userLoggedKey)
        keychain.delete(AppAuthHandler.userIdKey)
    }
}
