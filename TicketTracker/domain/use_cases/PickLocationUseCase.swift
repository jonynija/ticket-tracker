//
//  PickLocationUseCase.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import Foundation

protocol PickLocationUseCase {
    func lookForLocation(address: String, completion: @escaping (Result<PredictionsResponse, Error>) -> Void)
    func getPlaceDetails(placeId: String, completion: @escaping (Result<PlaceResponse, Error>) -> Void)
}

class PickLocationInteractor: PickLocationUseCase {
    private let repository: PickLocationRepository

    init(repository: PickLocationRepository) {
        self.repository = repository
    }

    func lookForLocation(address: String, completion: @escaping (Result<PredictionsResponse, Error>) -> Void) {
        repository.lookForLocation(address: address, completion: completion)
    }
    
    func getPlaceDetails(placeId: String, completion: @escaping (Result<PlaceResponse, Error>) -> Void){
        repository.getPlaceDetails(placeId: placeId, completion: completion)
    }
}
