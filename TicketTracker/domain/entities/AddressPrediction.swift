//
//  Predictions.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation

struct AddressPrediction: Decodable {
    let desc: String
    let placeId: String
    
    enum CodingKeys: String, CodingKey {
           case desc = "description"
           case placeId = "place_id"
       }
}
