//
//  AppLocation.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation

struct AppLocation: Decodable {
    let latitude: Double
    let longitude: Double
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
}
