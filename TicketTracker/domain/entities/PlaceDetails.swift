//
//  PlaceDetails.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import Foundation

struct Geometry: Decodable {
    let location: AppLocation
}
