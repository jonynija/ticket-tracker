//
//  UpdateTicketBloc.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 10/01/24.
//

import Foundation
import NotificationBannerSwift

class UpdateTicketView: ObservableObject {
    
    @Published var clientNameController: String = ""
    @Published var addressController: String = ""
    @Published var appointmentDate: Date = Date()
    
    let ticket: Ticket
    
    init(ticket: Ticket){
        self.ticket = ticket
        
        clientNameController = ticket.client
        addressController = ticket.address
        appointmentDate = ticket.date
    }
    
    let dateRange: ClosedRange<Date> = {
        let calendar = Calendar.current
        let now = Date()
        
        let tomorrow = calendar.date(byAdding: .day, value: 1, to: now)!
        let maxOneYearFromNow = calendar.date(byAdding: .year, value: 1, to: now)!
        
        return tomorrow...maxOneYearFromNow
    }()
    
    func isValidInfo() -> Bool {
        return !clientNameController.trim().isEmpty &&
        !addressController.trim().isEmpty
    }
    
    func updateTicket() -> Bool {
        if !isValidInfo() {
            NotificationBanner(subtitle: "Please fill the form", style: .warning).show()
            return false
        }
        let ticket = buildTicket()
        
        let isUpdated = DatabaseManager.shared.tickets?.update(ticket: ticket) ?? false
        
        
        if isUpdated {
            NotificationBanner(subtitle: "Ticket updated", style: .success).show()
            return true
        }
        
        NotificationBanner(subtitle: "An error ocurred, try again", style: .danger).show()
        return false
    }
    
    private func buildTicket() -> Ticket{
        return  Ticket(
            id: ticket.id,
            date: appointmentDate,
            client: clientNameController,
            address: addressController
        )
    }
}
