//
//  UpdateTicketPage.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 10/01/24.
//

import SwiftUI

struct UpdateTicketPage: View {
    
    @Environment(\.dismiss) var dismiss
    
    let onTicketUpdate: () -> Void
    @ObservedObject var updateTicketView: UpdateTicketView
    
    init(ticket: Ticket, onTicketUpdate: @escaping () -> Void) {
        self.onTicketUpdate = onTicketUpdate
        self.updateTicketView = UpdateTicketView(ticket: ticket)
    }
    
    var body: some View {
        NavigationStack {
            ScrollView{
                VStack {
                    AppInput(
                        content: $updateTicketView.clientNameController,
                        title: "Client"
                    )
                    AppInput(
                        content: $updateTicketView.addressController,
                        title: "Address"
                    )
                    
                    DatePicker(
                        selection: $updateTicketView.appointmentDate,
                        in: updateTicketView.dateRange,
                        displayedComponents: [.date, .hourAndMinute]
                    ) {
                        Text("Appointment date:")
                    }
                    .padding(.bottom)

                    AppButton(title: "Update") {
                        let isTicketUpdated = updateTicketView.updateTicket()
                        
                        if isTicketUpdated {
                            dismiss()
                            
                            onTicketUpdate()
                        }
                    }
                }
                .padding()
            }
            .navigationBarTitle("Update Ticket")
            .navigationBarItems(
                trailing: Button("Close", action: {
                    dismiss()
                })
            )
        }
    }
}

#Preview {
    UpdateTicketPage(
        ticket: Ticket(
            id: 0,
            date: Date(),
            client: "Lorem ipsum",
            address: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        )
    ){
        
    }
}
