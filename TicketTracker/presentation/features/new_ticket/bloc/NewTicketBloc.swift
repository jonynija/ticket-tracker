//
//  NewTicketBloc.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation
import NotificationBannerSwift

class NewTicketbloc: ObservableObject {
    
    @Published var clientNameController: String = ""
    @Published var addressController: String = ""
    @Published var appointmentDate: Date = Date()
    
    
    init(){
        appointmentDate = Calendar.current.date(byAdding: .day, value: 1, to: appointmentDate) ?? appointmentDate
    }
    
    let dateRange: ClosedRange<Date> = {
        let calendar = Calendar.current
        let now = Date()
        
        let tomorrow = calendar.date(byAdding: .day, value: 1, to: now)!
        let maxOneYearFromNow = calendar.date(byAdding: .year, value: 1, to: now)!
        
        return tomorrow...maxOneYearFromNow
    }()
    
    func isValidInfo() -> Bool {
        return !clientNameController.trim().isEmpty &&
        !addressController.trim().isEmpty
    }
    
    func createTicket() -> Ticket? {
        if !isValidInfo() {
            NotificationBanner(subtitle: "Please fill the form", style: .warning).show()
            return nil
        }
        
        let ticket = buildTicket(id: nil)
        let id = DatabaseManager.shared.tickets?.insert(ticket: ticket)
        
        if id == nil {
            NotificationBanner(subtitle: "An error ocurred, try again", style: .danger).show()
            return nil
        }
        
        NotificationBanner(subtitle: "Ticket created", style: .success).show()
        return buildTicket(id: id)
    }
    
    private func buildTicket(id: Int64?) -> Ticket{
        return  Ticket(
            id: id ?? -1,
            date: appointmentDate,
            client: clientNameController,
            address: addressController
        )
    }
    
}
