//
//  NewTicketScreen.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import SwiftUI

struct NewTicketScreen: View {
    @Environment(\.dismiss) var dismiss
    
    let onTicketCreated: (Ticket) -> Void
    @ObservedObject var newTicketBloc: NewTicketbloc = NewTicketbloc()
    
    var body: some View {
        NavigationStack {
            ScrollView{
                VStack {
                    AppInput(
                        content: $newTicketBloc.clientNameController,
                        title: "Client"
                    )
                    AppInput(
                        content: $newTicketBloc.addressController,
                        title: "Address"
                    )
                    
                    DatePicker(
                        selection: $newTicketBloc.appointmentDate,
                        in: newTicketBloc.dateRange,
                        displayedComponents: [.date, .hourAndMinute]
                    ) {
                        Text("Appointment date:")
                    }
                    .padding(.bottom)

                    AppButton(title: "Save") {
                        let ticketCreated = newTicketBloc.createTicket()
                        
                        if ticketCreated != nil {
                            dismiss()
                            
                            onTicketCreated(ticketCreated!)
                        }
                    }
                }
                .padding()
            }
            .navigationBarTitle("New Ticket")
            .navigationBarItems(
                trailing: Button("Close", action: {
                    dismiss()
                })
            )
        }
    }
}

#Preview {
    NewTicketScreen { ticket in
        
    }
}
