//
//  TicketsPage.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 7/01/24.
//

import SwiftUI

struct TicketsPage: View {
    
    let checkAuth: () -> Void
    @ObservedObject var ticketBloc: TicketsBloc = TicketsBloc()
    
    var body: some View {
        NavigationStack {
            List {
                ForEach(ticketBloc.tickets, id: \.id ){ ticket in
                    TicketItem(
                        ticket: ticket,
                        onTap: {
                            ticketBloc.navigateToTicket(ticket)
                        },
                        onDeleteTap: {
                            ticketBloc.onDeleteTicket(ticket)
                        },
                        onUpdateTap: {
                            ticketBloc.onEditTicket(ticket)
                        }
                    )
                    .contentShape(Rectangle())
                    .onTapGesture {
                        ticketBloc.navigateToTicket(ticket)
                    }
                }
            }
            .navigationBarTitle("Dashboard")
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarLeading) {
                    Button(
                        action: {
                            ticketBloc.isCalendarPresented = true
                        },
                        label: {
                            Image(systemName: "calendar")
                                .padding()
                        }
                    )
                    
                    Button(
                        action: {
                            ticketBloc.getAllTickets()
                        },
                        label: {
                            Image(systemName: "arrow.clockwise")
                                .padding()
                        }
                    )
                }
                
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    Button(
                        action: {
                            ticketBloc.isNewTicketPresented = true
                        },
                        label: {
                            Image(systemName: "plus.circle")
                                .padding()
                        }
                    )
                }
                
                ToolbarItemGroup(placement: .secondaryAction) {
                    Button("Work Ticket") {
                        ticketBloc.navigateToTicket(nil)
                    }
                    
                    Button("Get Directions") {
                        ticketBloc.navigateToDirections()
                    }
                    
                    Button("Logout") {
                        ticketBloc.logout()
                        checkAuth()
                    }
                }
            }
            .sheet(
                isPresented: $ticketBloc.isNewTicketPresented
            ) {
                NewTicketScreen{ ticket in
                    ticketBloc.onNewTicketCreated(ticket: ticket)
                }
            }
            .sheet(
                isPresented: $ticketBloc.isUpdatePresented
            ) {
                if let currentTicket = ticketBloc.currentTicket {
                    UpdateTicketPage(ticket: currentTicket) {
                        ticketBloc.getAllTickets()
                    }
                } else {
                    EmptyView()
                }
            }
            .sheet(
                isPresented: $ticketBloc.isCalendarPresented
            ) {
                CalendarTicketsSheet(
                    highlightedDates: $ticketBloc.highlightedDates
                )
            }
            .navigationDestination(isPresented: $ticketBloc.navigateToTicketDetail) {
                if let currentTicket = ticketBloc.currentTicket{
                    TicketDetailPage(ticket: currentTicket)
                } else {
                    EmptyView()
                }
            }
            .navigationDestination(isPresented: $ticketBloc.navigateToGetDirections) {
                GetDirectionsPage(ticket: nil)
            }
        }
    }
}

#Preview {
    TicketsPage{
        
    }
}
