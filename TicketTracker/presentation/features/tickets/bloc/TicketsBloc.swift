//
//  Ticketsbloc.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation
import NotificationBannerSwift

class TicketsBloc: ObservableObject {
    
    @Published var tickets: [Ticket] = []
    @Published var highlightedDates: [Event] = []
    
    // MARK: Navigation vars
    @Published var isNewTicketPresented: Bool = false
    @Published var isCalendarPresented: Bool = false
    @Published var isUpdatePresented: Bool = false
    @Published var navigateToTicketDetail: Bool = false
    @Published var navigateToGetDirections: Bool = false
    
    @Published var currentTicket: Ticket?
    
    
    init() {
        getAllTickets()
    }
    
    
    func getAllTickets(){
        tickets = DatabaseManager.shared.tickets?.getAllTickets() ?? []
        tickets = tickets.sorted { $0.date < $1.date }
        
        highlightedDates = []
        
        tickets.forEach { ticket in
            let event = Event(date: ticket.date, note: ticket.client)
            highlightedDates.append(event)
        }
    }
    
    func navigateToTicket(_ ticket: Ticket?){
        if let ticket = ticket ?? tickets.last {
            navigateToTicketDetail = true
            currentTicket = ticket
            
            return
        }
    }
    
    func onEditTicket(_ ticket: Ticket?){
        currentTicket = ticket
        isUpdatePresented = true
    }
    
    func navigateToDirections(){
        navigateToGetDirections = true
    }
    
    func logout(){
        AppAuthHandler.shared.logout()
    }
    
    func onDeleteTicket(_ ticket: Ticket){
        let isDeleted = DatabaseManager.shared.tickets?.delete(id: ticket.id) ?? false
        
        if isDeleted {
            NotificationBanner(subtitle: "Ticked deleted", style: .success).show()
            getAllTickets()
        }else{
            NotificationBanner(subtitle: "An error ocurred, try again", style: .danger).show()
        }
    }
    
    func onNewTicketCreated(ticket: Ticket) {
        self.getAllTickets()
        self.navigateToTicket(ticket)
    }
}
