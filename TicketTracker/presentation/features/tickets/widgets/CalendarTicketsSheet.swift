//
//  CalendarTicketsSheet.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 10/01/24.
//

import SwiftUI
import UIKit

// MARK: Calendar inspired from
// https://github.com/StewartLynch/UICalendarView_SwiftUI-Completed

// Main view responsible for displaying the calendar sheet
struct CalendarTicketsSheet: View {
    
    @Binding var highlightedDates: [Event]
    
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationStack {
            VStack {
                CalendarView(
                    interval: DateInterval(start: .distantPast, end: .distantFuture),
                    highlightedDates: $highlightedDates
                )
            }
            .navigationTitle("Appointments")
            .navigationBarItems(
                trailing: Button("Close", action: {
                    dismiss()
                })
            )
        }
    }
}

// UIViewRepresentable for wrapping a UIKit calendar view in SwiftUI
struct CalendarView: UIViewRepresentable {
    
    let interval: DateInterval
    @Binding var highlightedDates: [Event]
    
    func makeUIView(context: Context) -> some UICalendarView {
        let view = UICalendarView()
        view.delegate = context.coordinator
        view.calendar = Calendar(identifier: .gregorian)
        view.availableDateRange = interval
        let dateSelection = UICalendarSelectionSingleDate(delegate: context.coordinator)
        view.selectionBehavior = dateSelection
        return view
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(parent: self, highlightedDates: $highlightedDates)
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        // Handle any updates if needed
    }
}

// Coordinator class to act as the delegate for the UICalendarView
class Coordinator: NSObject, UICalendarViewDelegate, UICalendarSelectionSingleDateDelegate {
    
    var parent: CalendarView
    @Binding var highlightedDates: [Event]
    
    init(parent: CalendarView, highlightedDates: Binding<[Event]>) {
        self.parent = parent
        self._highlightedDates = highlightedDates
    }
    
    @MainActor
    func calendarView(_ calendarView: UICalendarView, decorationFor dateComponents: DateComponents) -> UICalendarView.Decoration? {
        // Filter events for the given dateComponents
        let foundEvents = highlightedDates.filter { $0.date.startOfDay == dateComponents.date?.startOfDay }
        guard !foundEvents.isEmpty else { return nil }
        
        // Return a custom view with the count of events and a blue text color
        return .customView {
            let icon = UILabel()
            icon.text = "📅x\(foundEvents.count)"
            icon.textColor = .blue
            return icon
        }
    }
    
    func dateSelection(_ selection: UICalendarSelectionSingleDate, didSelectDate dateComponents: DateComponents?) {
        // Handle date selection if needed
    }
    
    func dateSelection(_ selection: UICalendarSelectionSingleDate, canSelectDate dateComponents: DateComponents?) -> Bool {
        // Return true or false based on your selection criteria
        return true
    }
}

// Model representing an event
struct Event: Identifiable {
    var date: Date
    var note: String
    var id: String
    
    init(id: String = UUID().uuidString, date: Date, note: String) {
        self.date = date
        self.note = note
        self.id = id
    }
}

// Extension on Date to get the start of the day
extension Date {
    var startOfDay: Date {
        Calendar.current.startOfDay(for: self)
    }
}
