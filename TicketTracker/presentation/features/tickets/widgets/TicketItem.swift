//
//  TicketItem.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import SwiftUI

struct TicketItem: View {
    let ticket: Ticket
    let onTap: () -> Void
    let onDeleteTap: () -> Void
    let onUpdateTap: () -> Void
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            HStack {
                Text(ticket.client)
                    .font(.headline)
                    .foregroundColor(.primary)
                
                Spacer()
                
                Button("View Ticket") {
                    onTap()
                }
                .padding()
            }
            
            VStack(alignment: .leading, spacing: 4) {
                TicketInfoRow(title: "Hour", value: ticket.formatHour())
                TicketInfoRow(title: "Date", value: ticket.formatDate())
                TicketInfoRow(title: "Address", value: ticket.address)
            }
        }
        .swipeActions {
            Button("Delete") {
                onDeleteTap()
            }
            .tint(.red)
            
            Button("Update") {
                onUpdateTap()
            }
            .tint(.blue)
        }
    }
}

struct TicketInfoRow: View {
    let title: String
    let value: String
    
    var body: some View {
        HStack(alignment: .firstTextBaseline) {
            Text("\(title):")
                .font(.caption)
                .foregroundColor(.secondary)
            
            Text(value)
                .font(.body)
                .foregroundColor(.primary)
        }
    }
}


#Preview {
    List{
        TicketItem(
            ticket: Ticket(
                id: 0,
                date: Date(),
                client: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                address: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            ),
            onTap: {
                
            },
            onDeleteTap: {
                
            },
            onUpdateTap: {
                
            }
        )
    }
}
