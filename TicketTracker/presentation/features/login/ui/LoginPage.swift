//
//  LoginPage.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 7/01/24.
//

import SwiftUI

struct LoginPage: View {
    
    let checkAuth: () -> Void
    @ObservedObject var loginBloc: LoginBloc = LoginBloc()
    
    var body: some View {
        NavigationStack{
            VStack(alignment: .center) {
                Spacer()
                
                AppInput(
                    content: $loginBloc.usernameController,
                    title: "Username"
                )
                
                AppPasswordInput(
                    content: $loginBloc.passwordController,
                    title: "Password"
                )
                
                AppButton(title: "Login") {
                    let isUserLogged = loginBloc.onLogin()
                    if isUserLogged {
                        checkAuth()
                    }
                }
                .padding(.bottom, 40)
                
                AppButton(title: "Register") {
                    loginBloc.displayRegister = true
                }
                
                Spacer()
            }
            .navigationTitle("Login")
            .padding(16)
            .sheet(
                isPresented: $loginBloc.displayRegister,
                content: {
                    RegisterPage(
                        onRegister: {
                            checkAuth()
                        }
                    )
                }
            )
        }
    }
}

#Preview {
    LoginPage {
        
    }
}
