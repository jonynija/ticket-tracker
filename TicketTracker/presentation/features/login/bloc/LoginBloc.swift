//
//  LoginBloc.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 7/01/24.
//

import Foundation
import NotificationBannerSwift

class LoginBloc: ObservableObject {
    
    @Published var usernameController: String = ""
    @Published var passwordController: String = ""
    
    @Published var displayRegister = false
    
    
    func onLogin() -> Bool {
        if usernameController.trim().isEmpty || passwordController.trim().isEmpty {
            NotificationBanner(subtitle: "Please add your credentials", style: .warning).show()
            return false
        }
        
        let user = DatabaseManager.shared.users?.getUser(
            username: usernameController.trim(),
            password: passwordController.trim()
        )
        
        if let id = user?.id {
            AppAuthHandler.shared.saveUser(id: id)
            return true
        }
        
        NotificationBanner(subtitle: "No user were found", style: .danger).show()
        return false
    }
    
}
