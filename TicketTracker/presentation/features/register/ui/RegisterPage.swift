//
//  RegisterPage.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import SwiftUI

struct RegisterPage: View {
    
    @Environment(\.dismiss) var dismiss
    
    let onRegister: () -> Void
    @ObservedObject var registerBloc: RegisterBloc = RegisterBloc()
    
    var body: some View {
        NavigationStack{
            VStack(alignment: .center) {
                Spacer()
                
                AppInput(
                    content: $registerBloc.usernameController,
                    title: "Username"
                )
                
                AppPasswordInput(
                    content: $registerBloc.passwordController,
                    title: "Password"
                )
                
                AppButton(title: "Register") {
                    let isUserLogged = registerBloc.onRegister()
                    if isUserLogged {
                        dismiss()
                        onRegister()
                    }
                }
                
                Spacer()
            }
            .navigationTitle("Register")
            .navigationBarItems(
                trailing: Button("Close", action: {
                    dismiss()
                })
            )
            .padding(16)
        }
    }
}

#Preview {
    RegisterPage(
        onRegister: {
            
        }
    )
}
