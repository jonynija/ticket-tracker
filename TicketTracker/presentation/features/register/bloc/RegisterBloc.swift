//
//  RegisterBloc.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import Foundation
import KeychainSwift
import NotificationBannerSwift

class RegisterBloc: ObservableObject {
    
    @Published var usernameController: String = ""
    @Published var passwordController: String = ""
    
    
    func onRegister() -> Bool {
        if usernameController.trim().isEmpty || passwordController.trim().isEmpty {
            NotificationBanner(subtitle: "Please add your credentials", style: .warning).show()
            return false
        }
        
        let id = DatabaseManager.shared.users?.createUser(
            username: usernameController.trim(),
            password: passwordController.trim()
        )
        
        if let id = id {
            AppAuthHandler.shared.saveUser(id: id)
            return true
        }
        
        NotificationBanner(subtitle: "An user already exists with the same username", style: .danger).show()
        return false
    }
}
