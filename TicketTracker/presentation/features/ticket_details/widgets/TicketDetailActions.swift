//
//  TicketDetailActions.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import SwiftUI

struct TicketDetailActions: View {
    
    let onOverviewTap: () -> Void
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text("Reason for Call")
                .font(.headline)
            
            // Add your content for Reason for Call here
            
            // Interactive Buttons
            if UIDevice.current.userInterfaceIdiom == .pad ||
                (UIDevice.current.userInterfaceIdiom == .phone &&
                 UIScreen.main.bounds.width > UIScreen.main.bounds.height) {
                // iPad or Landscape mode on iPhone
                HStack(spacing: 16) {
                    AppButton(title: "Overview") {
                        onOverviewTap()
                    }
                    AppButton(title: "Work Details") {
                    }
                    AppButton(title: "Purchasing") {
                    }
                    AppButton(title: "Finishing Up") {
                    }
                    CameraButton{
                    }
                }
                .padding()
                .frame(maxWidth: .infinity)
                .background(Color.clear) // Add background color if needed
            } else {
                // Portrait mode on iPhone
                VStack(spacing: 16) {
                    AppButton(title: "Overview") {
                        onOverviewTap()
                    }
                    AppButton(title: "Work Details") {
                    }
                    AppButton(title: "Purchasing") {
                    }
                    AppButton(title: "Finishing Up") {
                    }
                    CameraButton{
                    }
                }
                .frame(maxWidth: .infinity)
                .background(Color.clear)
            }
        }
    }
}

struct CameraButton: View {
    let onTap: () -> Void
    
    var body: some View {
        Button(
            action: {
                onTap()
            },
            label: {
                Image(systemName: "camera")
                    .font(.system(size: 18, weight: .bold))
                    .foregroundColor(.white)
                    .padding(.vertical, 12)
                    .padding(.horizontal, 24)
                    .frame(maxWidth: .infinity)
                    .background(
                        LinearGradient(
                            gradient:
                                Gradient(
                                    colors: [
                                        Color.blue,
                                        Color.gray,
                                    ]
                                ),
                            startPoint: .leading,
                            endPoint: .trailing
                        )
                        .cornerRadius(5)
                    )
            }
        )
    }
}

#Preview {
    TicketDetailActions(
        onOverviewTap: {
            
        }
    )
}
