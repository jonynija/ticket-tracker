//
//  TicketDetailPage.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import SwiftUI

struct TicketDetailPage: View {
    let ticket: Ticket
    
    init(ticket: Ticket) {
        self.ticket = ticket
        self.ticketDetailBloc = TicketDetailBloc(ticket: ticket)
    }
    
    @ObservedObject var ticketDetailBloc: TicketDetailBloc
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 16) {
                // Customer Info
                VStack(alignment: .leading, spacing: 8) {
                    Text("Customer Info")
                        .font(.headline)
                    
                    Text("Name: \(ticket.client)")
                    Text("Phone: +502 50733337")
                    HStack {
                        Text("Address: \(ticket.address)")
                        Spacer()
                        Button("Get Directions") {
                            ticketDetailBloc.navigateToGetDirections = true
                        }
                    }
                }
                
                // Scheduled For
                HStack {
                    Spacer()
                    
                    Text("Scheduled For: \(ticket.formatDate()) at \(ticket.formatHour())")
                        .font(.title)
                    
                    Spacer()
                }
                .padding(.vertical)
                
                // Notes
                AppInput(
                    content: $ticketDetailBloc.notesController,
                    title: "Notes"
                )
                
                // Reason for Call
                TicketDetailActions(
                    onOverviewTap: {
                        
                    }
                )
            }
            .padding()
        }
        .navigationBarTitle("Ticket \(ticket.client)")
        .toolbar {
            ToolbarItemGroup(placement: .secondaryAction) {
                Button("Dashboard") {
                    self.presentationMode.wrappedValue.dismiss()
                }
                
                Button("Get Directions") {
                    ticketDetailBloc.navigateToGetDirections = true
                }
            }
        }
        .navigationDestination(isPresented: $ticketDetailBloc.navigateToGetDirections) {
            GetDirectionsPage(ticket: ticketDetailBloc.ticket)
        }
    }
}

struct OverviewPageView: View {
    var body: some View {
        // Implement the content for the Overview page here
        Text("Overview Page")
    }
}

#Preview {
    TicketDetailPage(
        ticket: Ticket(
            id: 0,
            date: Date(),
            client: "Lorem ipsum",
            address: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        )
    )
}
