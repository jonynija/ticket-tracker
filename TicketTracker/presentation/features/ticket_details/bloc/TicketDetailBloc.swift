//
//  TicketDetailBloc.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation

class TicketDetailBloc: ObservableObject {
    
    
    init(ticket: Ticket) {
        self.ticket = ticket
    }
    
    @Published var navigateToGetDirections = false
    
    @Published var ticket: Ticket
    
    // MARK: not current used
    @Published var notesController: String = ""
    
}
