//
//  GetDirectionsPage.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import SwiftUI
import MapKit

struct GetDirectionsPage: View {
    let ticket: Ticket?
    
    init(ticket: Ticket?) {
        self.ticket = ticket
        self.getDirectionsBloc = GetDirectionsBloc(ticket: ticket)
    }
    
    @ObservedObject var getDirectionsBloc: GetDirectionsBloc
    
    var body: some View {
        ZStack (alignment: .topLeading) {
            // Map View
            GoogleMapsView(
                markers: $getDirectionsBloc.markers,
                mapView: $getDirectionsBloc.mapController
            )
            .ignoresSafeArea(edges: .all)
            
            // Overlay Panel
            ZStack (alignment: .topLeading) {
                AppBlur(style: .light)
                
                VStack {
                    DirectionsSearchBar(
                        content: $getDirectionsBloc.searchController,
                        onSearchTap: {
                            getDirectionsBloc.searchLocations()
                        },
                        onClearTap: {
                            getDirectionsBloc.clearList()
                        }
                    )
                    
                    if !getDirectionsBloc.predictions.isEmpty{
                        ScrollView {
                            ForEach(getDirectionsBloc.predictions, id: \.placeId ){ prediction in
                                PredictionItem(
                                    prediction: prediction,
                                    onPredictionTap: {
                                        getDirectionsBloc.getPredictionDetail(prediction: prediction)
                                    }
                                )
                            }
                        }
                        .padding(.top)
                    }
                }
            }
            .cornerRadius(12)
            .frame(minWidth: 300, maxWidth: 500, minHeight: 0, maxHeight: $getDirectionsBloc.predictions.isEmpty ? 80 : 300 )
            .padding()
        }
        .navigationTitle("Get Directions")
    }
}

#Preview {
    NavigationStack{
        GetDirectionsPage(
            ticket: nil
        )
    }
}


