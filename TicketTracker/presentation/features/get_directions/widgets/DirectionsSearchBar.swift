//
//  DirectionsSearchBar.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import SwiftUI

struct DirectionsSearchBar: View {
    
    @Binding var content: String
    let onSearchTap: () -> Void
    let onClearTap: () -> Void
    
    var body: some View {
        HStack(alignment: .center) {
            ZStack(alignment: .trailing) {
                TextField("", text: $content)
                    .keyboardType(.default)
                    .onSubmit {
                        onSearchTap()
                    }
                    .padding(.all, 10)
                    .cornerRadius(5)
                    .overlay(
                        RoundedRectangle(cornerRadius: 5)
                            .stroke(
                                LinearGradient(
                                    gradient: Gradient(
                                        colors: [
                                            Color.blue,
                                            Color.gray,
                                        ]
                                    ),
                                    startPoint: .topLeading,
                                    endPoint: .bottomTrailing
                                ),
                                lineWidth: 2
                            )
                    )
                    .frame(maxWidth: .infinity)
                    .frame(height: 45)
                
                
                Button(action: onClearTap) {
                    Image(systemName: "clear")
                        .foregroundColor(.blue)
                }
                .padding(.trailing)
            }
            
            AppButton(title: "Search") {
                onSearchTap()
            }
            .frame(width: 100, height: 45)
        }
        .padding([.horizontal, .top])
    }
}

#Preview {
    DirectionsSearchBar(
        content: .constant(""),
        onSearchTap: {
            print("Searching")
        },
        onClearTap: {
            
        }
    )
}
