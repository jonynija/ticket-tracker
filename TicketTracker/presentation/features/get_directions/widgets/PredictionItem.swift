//
//  PredictionItem.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import SwiftUI

struct PredictionItem: View {
    let prediction: AddressPrediction
    let onPredictionTap: () -> Void
    
    var body: some View {
        VStack {
            Text(
                prediction.desc
            )
            .multilineTextAlignment(.center)
            .frame(maxWidth: .infinity)
            
            Divider()
                .frame(height: 1)
        }
        .frame(maxWidth: .infinity)
        .padding(.horizontal)
        .padding(.vertical, 4)
        .onTapGesture {
            onPredictionTap()
        }
    }
}

#Preview {
    PredictionItem(
        prediction: AddressPrediction(desc: "Guatemala Guatemala", placeId: "asdasdasdasd"),
        onPredictionTap: {
            
        }
    )
}
