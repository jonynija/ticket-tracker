//
//  GetDirectionsBloc.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation
import GoogleMaps
import NotificationBannerSwift

class GetDirectionsBloc: ObservableObject {
    
    init(ticket: Ticket?) {
        self.ticket = ticket
        
        let pickLocationRepository = PickLocationRepositoryImpl(networkManager: NetworkManager.shared)
        self.pickLocationUseCase = PickLocationInteractor(repository: pickLocationRepository)
        
        // Look for location if navigates from ticket details
        self.searchController = ticket?.address ?? ""
    }
    
    let ticket: Ticket?
    
    private let pickLocationUseCase: PickLocationUseCase
    
    @Published var searchController: String = ""
    @Published var predictions: [AddressPrediction] = []
    @Published var markers: [GMSMarker] = []
    
    @Published var mapController: MapViewController = MapViewController()
    
    func searchLocations() {
        if searchController.trim().isEmpty {
            NotificationBanner(subtitle: "Fill the form", style: .warning).show()
            return
        }
        
        pickLocationUseCase.lookForLocation(address: searchController.trim()) { result in
            switch result {
            case .success(let result):
                self.predictions = result.predictions
                if self.predictions.isEmpty {
                    NotificationBanner(subtitle: "There were no direcctions found", style: .danger).show()
                }
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func clearList(){
        predictions = []
    }
    
    func getPredictionDetail(prediction: AddressPrediction){
        pickLocationUseCase.getPlaceDetails(placeId: prediction.placeId) { result in
            switch result {
            case .success(let result):
                self.createMarker(prediction, response: result)
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    private func createMarker(_ prediction: AddressPrediction,response: PlaceResponse){
        if let location = response.results.first?.geometry.location {
            let coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            
            let marker =  GMSMarker(position: coordinate)
            marker.title = prediction.desc
            
            markers = [marker]
            
            mapController.moveTo(location)
        } else {
            NotificationBanner(subtitle: "An unknown error ocurred", style: .danger).show()
        }
    }
    
}

