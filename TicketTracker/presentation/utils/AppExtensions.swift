//
//  AppExtensions.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 8/01/24.
//

import Foundation

extension String {
    func trim() -> String {
    return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
   }
}
