//
//  AppButton.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 7/01/24.
//

import SwiftUI

struct AppButton: View {
    let title: String
    let action: () -> Void
    
    var body: some View {
        Button(
            action: action,
            label: {
                Text(title)
                    .font(.system(size: 18, weight: .bold))
                    .foregroundColor(.white)
                    .padding(.vertical, 12)
                    .padding(.horizontal, 6)
                    .frame(maxWidth: .infinity)
                    .background(
                        LinearGradient(
                            gradient:
                                Gradient(
                                    colors: [
                                        Color.blue,
                                        Color.gray,
                                    ]
                                ),
                            startPoint: .leading,
                            endPoint: .trailing
                        )
                        .cornerRadius(5)
                    )
            }
        )
        .buttonStyle(PlainButtonStyle())
    }
}

#Preview {
    AppButton(
        title: "Action",
        action: {}
    )
}
