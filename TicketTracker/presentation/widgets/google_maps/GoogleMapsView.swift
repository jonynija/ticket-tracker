//
//  GoogleMapsView.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 9/01/24.
//

import SwiftUI
import GoogleMaps

struct GoogleMapsView: UIViewControllerRepresentable {
    
    @Binding var markers: [GMSMarker]
    @Binding var mapView: MapViewController
    
    private let initialLocation = AppLocation(latitude: 45.2502798, longitude: -75.9650594)
    
    // MARK: Overrides
    
    func makeUIViewController(context: Context) -> MapViewController {
        mapView.moveTo(initialLocation)
        
        return mapView
    }
    
    func updateUIViewController(_ uiViewController: MapViewController, context: Context) {
        markers.forEach { $0.map = uiViewController.map }
    }
}

class MapViewController: UIViewController {
    
    let map = GMSMapView(frame: .zero)
    
    var isDarkModeEnabled: Bool? = nil
    private let zoom: Float = 15.0
    
    override func loadView() {
        super.loadView()
        self.view = map
        
        applyColorSchemeToMap()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        applyColorSchemeToMap()
    }
    
    
    private func applyColorSchemeToMap() {
        if (isDarkModeEnabled == isDarkMode){
            return
        }
        
        isDarkModeEnabled = isDarkMode
        
        do {
            let mapStyle = (isDarkMode) ? GoogleMapsUtils.darkModeStyle : GoogleMapsUtils.lightModeStyle
            
            map.mapStyle = try GMSMapStyle(jsonString: mapStyle)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    func moveTo(_ location: AppLocation) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.map.animate(toLocation: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.map.animate(toZoom: self.zoom)
            })
        }
    }
}

extension UIViewController {
    var isDarkMode: Bool {
        if #available(iOS 13.0, *) {
            return self.traitCollection.userInterfaceStyle == .dark
        }
        else {
            return false
        }
    }
    
}
