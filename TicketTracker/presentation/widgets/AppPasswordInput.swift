//
//  AppPasswordInput.swift
//  TicketTracker
//
//  Created by Jonathan Ixcayau on 7/01/24.
//

import SwiftUI

struct AppPasswordInput: View {
    @Binding var content: String
    let title: String
    
    var body: some View {
        VStack(alignment:.leading){
            Text(title)
                .foregroundColor(Color.primary)
            
            SecureField("", text: $content)
                .keyboardType(.default)
                .padding(.all, 10)
                .cornerRadius(5)
                .overlay(
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(
                            LinearGradient(
                                gradient: Gradient(
                                    colors: [
                                        Color.blue,
                                        Color.gray,
                                    ]
                                ),
                                startPoint: .topLeading,
                                endPoint: .bottomTrailing
                            ),
                            lineWidth: 2
                        )
                )
                .frame(maxWidth: .infinity)
                .frame(height: 45)
                .padding(.bottom,20)
        }
    }
}

#Preview {
    AppPasswordInput(
        content: .constant(""), title: "Password"
    )
}
